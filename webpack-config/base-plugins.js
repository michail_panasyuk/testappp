const path = require('path');
const webpack = require('webpack');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const extractSass = require('./extract-sass');

const plugins = [
  new CleanWebpackPlugin(['dist'], {
    root: __dirname,
    verbose: true,
    dry: false
  }),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production')
  }),
  new webpack.optimize.CommonsChunkPlugin({
    names: ['vendor', 'manifest']
  }),
  new webpack.HotModuleReplacementPlugin(),
  extractSass,
  new HtmlWebpackPlugin({
    filename: 'index.html',
    title: 'Plex Website',
    inject: 'body',
    template: path.resolve(__dirname, '..', 'application', 'index.html')
  })
];

module.exports = plugins;
