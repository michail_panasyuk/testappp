const path = require('path')

const extractSass = require('./extract-sass')

const eslintLoaderRule = {
  test: /\.js$/,
  enforce: 'pre',
  loader: 'eslint-loader',
  exclude: /node_modules/
}

const babelLoaderRule = {
  test: /\.js$/,
  loader: 'babel-loader',
  query: {
    presets: [
      'es2015',
      'react'
    ],
    plugins: []
  },
  include: [
    path.resolve(__dirname, '..', 'application')
  ]
}

const jsonLoaderRule = {
  test: /\.json$/,
  loader: 'json-loader'
}

const sassLoaderRule = {
  test: /\.scss$/,
  use: extractSass.extract({
    use: [
      {
        loader: 'css-loader'
      },
      {
        loader: 'sass-loader'
      }
    ],
    fallback: 'style-loader'
  })
}

const loaderRules = [
  eslintLoaderRule,
  babelLoaderRule,
  jsonLoaderRule,
  sassLoaderRule
]

module.exports = loaderRules
