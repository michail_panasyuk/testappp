const path = require('path')

const loaderRules = require('./webpack-config/base-loaders')
const plugins = require('./webpack-config/base-plugins')

const entry = {
  client: 'webpack-hot-middleware/client',
  app: path.join(__dirname, 'application', 'index.js'),
  vendor: [
    'react',
    'react-dom',
    'prop-types',
    'react-redux',
    'redux'
  ]
}

const output = {
  filename: '[name].[hash].js',
  path: path.resolve(__dirname, 'dist'),
  library: '[name]'
}

const config = {
  entry,
  output,
  module: { rules: loaderRules },
  plugins,
  resolve: {
    modules: [
      path.join(process.cwd(), 'app'),
      'node_modules'
    ],
    extensions: ['.js', '.json'],
    alias: {
      settings: path.join(__dirname, 'application', 'settings.json'),
      actions: path.join(__dirname, 'application', 'actions'),
      pages: path.join(__dirname, 'application', 'pages'),
      reducers: path.join(__dirname, 'application', 'reducers'),
      routes: path.join(__dirname, 'application', 'routes'),
      forms: path.join(__dirname, 'application', 'forms'),
      fields: path.join(__dirname, 'application', 'fields'),
      controls: path.join(__dirname, 'application', 'controls'),
      sagas: path.join(__dirname, 'application', 'sagas'),
      helpers: path.join(__dirname, 'application', 'helpers'),
      libs: path.join(__dirname, 'application', 'libs')
    }
  },
  devtool: 'eval-source-map',
  devServer: {
    historyApiFallback: true
  }
}

module.exports = config
