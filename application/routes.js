import React from 'react';
import PropTypes from 'prop-types';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import IndexPage from './pages/IndexPage';
import Layout from './pages/Layout';

const Routes = ({ store }) => {
  const history = syncHistoryWithStore(browserHistory, store);

  return (
    <Router history={history}>
      <Route path="/" component={Layout}>
        <Route path="/" component={IndexPage} />
      </Route>
    </Router>
  );
};

Routes.propTypes = {
  store: PropTypes.shape({}).isRequired
};

export default Routes;
