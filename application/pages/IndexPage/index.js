import React from 'react';
// import PropTypes from 'prop-types';

import { connect } from 'react-redux';

const IndexPage = () => <div className="page" />;

// IndexPage.propTypes = {};

// const mapStateToProps = state => ({});

const mapDispatchToProps = {};

const ConnectedIndexPage = connect(null, mapDispatchToProps)(
  IndexPage
);

export default ConnectedIndexPage;
