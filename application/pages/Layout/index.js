import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const Layout = ({ children }) => (
  <div className="wrapper">
    {children}
  </div>
);

Layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};

Layout.defaultProps = {
  children: null
};

export default Layout;
