import axios from 'axios';

const BASE_URL = 'http://api.openweathermap.org/data/2.5/';
const KEY = '3aa02e241c1987b6f127c101b258fe09';

function prepRouteParams(queryStringData) {
  return Object.keys(queryStringData)
    .map(key => `${key}=${encodeURIComponent(queryStringData[key])}`)
    .join('&');
}

function prepUrl(type, queryStringData) {
  return `${BASE_URL + type}?${prepRouteParams(queryStringData)}`;
}

function getQueryStringData(city) {
  return {
    q: city,
    type: 'accurate',
    APPID: KEY,
    cnt: 5
  };
}

export function getCurrentWeather(city) {
  const queryStringData = getQueryStringData(city);
  const url = prepUrl('weather', queryStringData);

  return axios.get(url).then(currentWeatherData => currentWeatherData.data);
}

export function getForecast(city) {
  const queryStringData = getQueryStringData(city);
  const url = prepUrl('forecast/daily', queryStringData);

  return axios.get(url).then(forecastData => forecastData.data);
}
