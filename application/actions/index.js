export const LOADING = 'LOADING';
export const LOADED = 'LOADED';

export const CITY_SELECTED = 'CITY_SELECTED';
export const DAY_VIEW_SELECTED = 'DAY_VIEW_SELECTED';
export const WEEK_VIEW_SELECTED = 'WEEK_VIEW_SELECTED';
export const MONTH_VIEW_SELECTED = 'MONTH_VIEW_SELECTED';

export const FETCH_DATA = 'FETCH_DATA';

export const FETCH_DATA_SUCCES = 'FETCH_DATA_SUCCES';
export const FETCH_DATA_EROROR = 'FETCH_DATA_EROR';

function citySelected(city) {
  return {
    type: CITY_SELECTED,
    city
  };
}
