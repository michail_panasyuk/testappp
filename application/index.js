/* global document */

import React from 'react';
import ReactDOM from 'react-dom';

import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import 'babel-core/register';
import 'babel-polyfill';

import Routes from './routes';
import reducers from './reducers';
// import settings from './settings.json';

const logger = createLogger({});

const store = createStore(
  reducers,
  applyMiddleware(thunk, logger)
);

const Application = () => (
  <Provider store={store}>
    <Routes store={store} />
  </Provider>
);

ReactDOM.render(<Application />, document.getElementById('application'));
